import react from 'react';
import './HeaderComponent.css';
import NavbarComponent from'./NavbarComponent'

const HeaderComponent = () => {
    return (
        <header className="headertop">
            <NavbarComponent/>

        </header>
    )
}

export default HeaderComponent;