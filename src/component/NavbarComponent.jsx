import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import "./FontAwesomeIcons/index";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const NavbarComponent = () =>{
  
    return (
        <Navbar className="NavbarTransparent" expand="lg">
          <Navbar.Brand href="#home">Widya Mustika</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ml-auto">
                <Nav.Link href="#about">About</Nav.Link>
                <Nav.Link href="#contact">Contact</Nav.Link>
                <Nav.Link href="#instagram">
                    <FontAwesomeIcon icon={['fab', 'instagram']} />
                </Nav.Link>
                <Nav.Link href="#facebook">
                    <FontAwesomeIcon icon={['fab', 'facebook']} />
                </Nav.Link>
                <Nav.Link href="#twitter">
                    <FontAwesomeIcon icon={['fab', 'twitter']} />
                </Nav.Link>
              </Nav>
          </Navbar.Collapse>
        </Navbar>
    )
}
export default NavbarComponent;